<?php declare(strict_types=1);

namespace Andry\CliChart\Drawer;

use Andry\CliChart\Domain\Drawer;
use Andry\CliChart\Domain\Point;
use Andry\CliChart\Domain\PointCollection;
use Andry\CliChart\Drawer\Config\PointOutputConfiguration;
use Andry\CliChart\Drawer\Config\XAxisConfiguration;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Output\OutputInterface;

final class CliDrawer implements Drawer
{
    public function __construct(
        private readonly OutputInterface $output,
        private readonly PointOutputConfiguration $pointConfig,
        private readonly XAxisConfiguration $axisConfig,
    ) {
        $this->output->getFormatter()->setStyle('point', new OutputFormatterStyle(
            $this->pointConfig->symbolColor,
            $this->pointConfig->backgroundColor
        ));
    }

    public function outputTitle(string $title): void
    {
        $this->output->writeln("");
        $this->output->writeln("CLI OUTPUT - $title");
        $this->output->writeln("");
        $this->output->writeln("");
    }

    public function drawPoints(PointCollection $collection): void
    {
        $drawAllocator = new XAxisPointsDrawAllocator($collection, $this->axisConfig);

        foreach ($collection->asArray() as $point) {
            $this->drawPoint($point, $drawAllocator->calculateSpacesAllocation($point));
        }
    }

    public function drawPoint(Point $point, int $spacesAllocation): void
    {
        $this->output->write($this->generateStringOfSpaces($spacesAllocation));
        $this->output->writeln(sprintf("<point>%s</point>", $this->pointConfig->symbol));
    }

    private function generateStringOfSpaces(int $count): string
    {
        return implode('', array_fill(0, $count, ' '));
    }
}