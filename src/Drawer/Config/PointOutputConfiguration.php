<?php declare(strict_types=1);

namespace Andry\CliChart\Drawer\Config;

final class PointOutputConfiguration
{
    private function __construct(
        public readonly ?string $symbol,
        public readonly ?string $symbolColor,
        public readonly ?string $backgroundColor,
    ) {
    }

    public static function create(
        string $symbol = null,
        string $symbolColor = null,
        string $backgroundColor = null,
    ): self {

        return new self($symbol ?? '.', $symbolColor ?? 'cyan', $backgroundColor);
    }
}