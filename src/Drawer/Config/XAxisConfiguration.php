<?php declare(strict_types=1);

namespace Andry\CliChart\Drawer\Config;

final class XAxisConfiguration
{
    public function __construct(
        public readonly int $lengthOfAxisX,
        public readonly int $xAxisOffset,
    ) {
    }

    public static function create(
        int $lengthOfAxisX = null,
        int $xAxisOffset = null,
    ): self {

        return new self(
            !empty($lengthOfAxisX) ? $lengthOfAxisX : 80,
            $xAxisOffset ?? 0,
        );
    }
}