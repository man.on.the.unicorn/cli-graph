<?php declare(strict_types=1);

namespace Andry\CliChart\Drawer;

use Andry\CliChart\Domain\Point;
use Andry\CliChart\Domain\PointCollection;
use Andry\CliChart\Drawer\Config\XAxisConfiguration;

final class XAxisPointsDrawAllocator
{
    public function __construct(
        private readonly PointCollection $collection,
        private readonly XAxisConfiguration $axisConfig,
    ) {
    }

    public function calculateSpacesAllocation(Point $point): int
    {
        return (int) ($this->axisConfig->xAxisOffset + (
            $this->axisConfig->lengthOfAxisX
            - (
                $this->pointDifferenceFromMaxCollectionValue($point) * $this->axisConfig->lengthOfAxisX
                / $this->collection->diffX()
            )
        ));
    }

    private function pointDifferenceFromMaxCollectionValue(Point $point): float
    {
        return $this->collection->maxX() - $point->x;
    }
}