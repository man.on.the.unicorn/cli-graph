<?php declare(strict_types=1);

namespace Andry\CliChart\Console;

use Andry\CliChart\Domain\Chart;
use Andry\CliChart\Domain\PointCollection;
use Andry\CliChart\Drawer\CliDrawer;
use Andry\CliChart\Drawer\Config\PointOutputConfiguration;
use Andry\CliChart\Drawer\Config\XAxisConfiguration;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

final class OutputChart extends Command
{
    private const INPUT_X_AXIS_JSON_ARRAY_OPTION_NAME = 'x-axis-json-input';
    private const POINT_SYMBOL_OPTION_NAME = 'point-symbol';
    private const POINT_SYMBOL_COLOR_OPTION_NAME = 'point-symbol-color';
    private const AXIS_X_LENGTH = 'x-axis-length';
    private const AXIS_X_OFFSET = 'x-axis-offset';

    protected static $defaultName = 'app:chart:draw-cli';

    protected function configure(): void
    {
        $this
            ->addOption(
                name: self::INPUT_X_AXIS_JSON_ARRAY_OPTION_NAME,
                mode: InputOption::VALUE_OPTIONAL,
                default: '[]',
            )
            ->addOption(
                name: self::POINT_SYMBOL_OPTION_NAME,
                mode: InputOption::VALUE_OPTIONAL,
            )
            ->addOption(
                name: self::POINT_SYMBOL_COLOR_OPTION_NAME,
                mode: InputOption::VALUE_OPTIONAL,
            )
            ->addOption(
                name: self::AXIS_X_LENGTH,
                mode: InputOption::VALUE_OPTIONAL,
            )
            ->addOption(
                name: self::AXIS_X_OFFSET,
                mode: InputOption::VALUE_OPTIONAL,
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $chart = new Chart(
            PointCollection::fromArrayAsXAxisCoords($this->getXChords($input)),
            new CliDrawer(
                $output,
                PointOutputConfiguration::create(
                    $input->getOption(self::POINT_SYMBOL_OPTION_NAME),
                    $input->getOption(self::POINT_SYMBOL_COLOR_OPTION_NAME),
                ),
                XAxisConfiguration::create(
                    (int) $input->getOption(self::AXIS_X_LENGTH),
                    (int) $input->getOption(self::AXIS_X_OFFSET),
                ),
            ),
        );

        $chart->draw();

        return 0;
    }

    /**
     * @return array<float>
     */
    private function getXChords(InputInterface $input): array
    {
        $value = $input->getOption(self::INPUT_X_AXIS_JSON_ARRAY_OPTION_NAME);

        return json_decode($value, true) ?: [];
    }
}