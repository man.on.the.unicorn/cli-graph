<?php declare(strict_types=1);

namespace Andry\CliChart\Domain;

final class Chart
{
    public function __construct(
        private readonly PointCollection $collection,
        private readonly Drawer $drawer,
    ) {
    }

    public function draw(): void
    {
        $this->drawer->outputTitle("Chart of point collection");
        $this->drawer->drawPoints($this->collection);
    }
}