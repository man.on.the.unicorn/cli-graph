<?php declare(strict_types=1);

namespace Andry\CliChart\Domain;

interface Drawer
{
    public function outputTitle(string $title): void;

    public function drawPoints(PointCollection $collection): void;

    public function drawPoint(Point $point, int $spacesAllocation): void;
}