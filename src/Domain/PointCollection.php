<?php declare(strict_types=1);

namespace Andry\CliChart\Domain;

final class PointCollection
{
    /**
     * @param array<Point> $points
     */
    public function __construct(private array $points)
    {
    }

    /**
     * @param array<float> $coords
     * @return static
     */
    public static function fromArrayAsXAxisCoords(array $coords): self
    {
        $points = [];

        foreach ($coords as $y => $x) {
            $points[] = new Point($x, $y);
        }

        return new self($points);
    }

    public function diffX(): float
    {
        return max($this->sequenceOfX()) - min($this->sequenceOfX());
    }

    public function maxX(): float
    {
        return max($this->sequenceOfX());
    }

    /**
     * @return array<float>
     */
    private function sequenceOfX(): array
    {
        return array_map(static fn (Point $p) => $p->x, $this->points);
    }

    public function asArray(): array
    {
        return $this->points;
    }
}