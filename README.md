# Cli Graph


## Requirements

- docker
- docker-compose

## Running application

```
docker-compose build
docker-compose run app
```

## Modifying options

Open docker-compose.yaml and modify input arguments/options

## Output example

![example](<./example/img.png>) 